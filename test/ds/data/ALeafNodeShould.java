package ds.data;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class ALeafNodeShould {

    @Test
    public void ProvideCorrectNodeID() {
        assertThat(new LeafNode(0, 0).getID(), equalTo(""));
        assertThat(new LeafNode(1, 0).getID(), equalTo("0"));
        assertThat(new LeafNode(1, 1).getID(), equalTo("1"));
        assertThat(new LeafNode(5, 31).getID(), equalTo("11111"));
        assertThat(new LeafNode(5, 0).getID(), equalTo("00000"));
    }

    @Test
    public void ProvideCorrectNumericNodeID() {
        assertThat(new LeafNode(0, 0).getNumericID(), equalTo(0));
        assertThat(new LeafNode(1, 0).getNumericID(), equalTo(256));
        assertThat(new LeafNode(1, 1).getNumericID(), equalTo(257));
        assertThat(new LeafNode(5, 31).getNumericID(), equalTo(1311));
        assertThat(new LeafNode(5, 0).getNumericID(), equalTo(1280));
    }

    @Test
    public void ProvideCorrectKeyFromNumericID() {
        assertThat(LeafNode.keyFromNumericID(0), equalTo(0));
        assertThat(LeafNode.keyFromNumericID(256), equalTo(0));
        assertThat(LeafNode.keyFromNumericID(257), equalTo(1));
        assertThat(LeafNode.keyFromNumericID(1311), equalTo(31));
    }

    @Test
    public void ProvideCorrectLevelFromNumbericID() {
        assertThat(LeafNode.levelFromNumericID(0), equalTo(0));
        assertThat(LeafNode.levelFromNumericID(256), equalTo(1));
        assertThat(LeafNode.levelFromNumericID(257), equalTo(1));
        assertThat(LeafNode.levelFromNumericID(1280), equalTo(5));
        assertThat(LeafNode.levelFromNumericID(1311), equalTo(5));
    }

    @Test
    public void ProvideCorrectParentNumericID() {
        assertThat(new LeafNode(0, 0).getParentNumericID(), equalTo(null));
        assertThat(new LeafNode(1, 0).getParentNumericID(), equalTo(0));
        assertThat(new LeafNode(1, 1).getParentNumericID(), equalTo(0));
        assertThat(new LeafNode(2, 0).getParentNumericID(), equalTo(256));
        assertThat(new LeafNode(2, 1).getParentNumericID(), equalTo(256 + 1));
        assertThat(new LeafNode(2, 2).getParentNumericID(), equalTo(256));
        assertThat(new LeafNode(2, 3).getParentNumericID(), equalTo(256 + 1));
        assertThat(new LeafNode(3, 5).getParentNumericID(), equalTo(512 + 1));
        assertThat(new LeafNode(3, 3).getParentNumericID(), equalTo(512 + 3));
        assertThat(new LeafNode(3, 4).getParentNumericID(), equalTo(512));
    }
}
