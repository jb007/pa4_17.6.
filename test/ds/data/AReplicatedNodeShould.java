package ds.data;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class AReplicatedNodeShould {

    @Test
    public void ProvideLocalNotesCorrectly() {
        Node node1 = new ReplicatedNode(0, 0, new InnerNode(2, 0, new LeafNode(3, 0), new LeafNode(3, 2)));
        Node node2 = new ReplicatedNode(0, 0, new InnerNode(2, 0,
                new ReplicatedNode(3, 0, new LeafNode(3, 0)),
                new ReplicatedNode(3, 2, new LeafNode(3, 2))));

        assertThat(node1.getLocalNodes().size(), equalTo(3));
        assertThat(node2.getLocalNodes().size(), equalTo(3));
    }
}
