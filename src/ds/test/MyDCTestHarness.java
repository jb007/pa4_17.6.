package ds.test;

import ds.client.MyDC;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;

import java.util.Random;
import java.util.UUID;
import java.util.Vector;

public class MyDCTestHarness {

    public static Options options = new Options();

    static {
        options.addOption(OptionBuilder.withLongOpt("remote-address")
                .withArgName("address")
                .hasArg()
                .isRequired()
                .create('A'));
        options.addOption(OptionBuilder.withLongOpt("remote-port")
                .withArgName("port")
                .hasArg()
                .withType(Number.class)
                .isRequired()
                .create('P'));
    }

    public static void main(String[] args) throws Exception {
        CommandLine cmd = new PosixParser().parse(options, args);

        String host = cmd.getOptionValue("remote-address");
        int port = ((Number) cmd.getParsedOptionValue("remote-port")).intValue();

        MyDCTestHarness testHarness = new MyDCTestHarness(new MyDC(host, port));
        testHarness.installShutdownHook();

        System.out.println("Ready (press Ctrl+C to quit)");

        testHarness.startTest();
    }


    /**
     * Installing signal handler for graceful shutdown.
     * The strange try-catch construct is necessary to be able to at least try to close all
     * connections. A single try-catch-block would prevent that.
     */
    private void installShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                myDC.shutdown();
                running = false;
            }
        });
    }


    private MyDC myDC;

    private boolean running = true;

    public MyDCTestHarness(MyDC myDC) {
        this.myDC = myDC;
    }

    public void startTest() {
        Random random = new Random();

        Vector<String> keys = new Vector<String>();

        long insertCounter = 0;
        long deleteCounter = 0;
        long lookupCounter = 0;
        while(running) {
            try {
                if (myDC.isReady()) {
                    int op = random.nextInt(5);

                    if (op == 0 || keys.size() == 0) {
                        insertCounter += 1;
                        String key = UUID.randomUUID().toString();
                        keys.add(key);
                        myDC.insert(key, key.substring(key.lastIndexOf("-") + 1));
                    } else if (op == 1) {
                        deleteCounter += 1;
                        String key = keys.elementAt(random.nextInt((keys.size())));
                        keys.remove(key);
                        myDC.delete(key);

//                        Thread.sleep(100);

                        if (myDC.lookup(key) != null) {
                            System.out.println("  ## ERROR ## " + key + " still exists");
                        }
                    } else {
                        lookupCounter += 1;
                        String key = keys.elementAt(random.nextInt((keys.size())));
                        String value = myDC.lookup(key);
                        if (!key.substring(key.lastIndexOf("-") + 1).equals(value)) {
                            System.out.println("  ## ERROR ## " + key + " -> " + myDC.lookup(key));
                        }
                    }
                }

                if ((insertCounter + deleteCounter + lookupCounter) % 100 == 0) {
                    System.out.println("#INSERT: " + insertCounter + "   #DELETE: " + deleteCounter + "   #LOOKUP: " + lookupCounter);
                }

                Thread.sleep(1000);
            } catch (InterruptedException e) {
                ; // Simply ignore exception
            }
        }
    }
}
