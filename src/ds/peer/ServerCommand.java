package ds.peer;

import com.esotericsoftware.kryonet.Connection;

/**
 * A <i>ServerCommand</i> is a {@link Command} which is executed by the server.
 * A connection prepares the necessary parameters sends it to the server who then performs the
 * corresponding action.
 */
public abstract class ServerCommand<P extends Peer, R extends RemotePeer> extends Command<P, R> {

    protected void sendResponse(Connection connection, P peer, Command cmd) {
        cmd.serverPort = peer.getPort();
        connection.sendTCP(cmd);
    }
}
