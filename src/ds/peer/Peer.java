package ds.peer;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import ds.peer.commands.JoinRequest;
import ds.peer.commands.JoinResponse;
import ds.peer.commands.Ping;
import ds.utils.KryoUtils;
import org.apache.commons.cli.*;

import java.io.IOException;
import java.net.InetAddress;
import java.util.*;

public class Peer {

    public static final int DEFAULT_PORT = 54001;

    public static boolean systemOutWithPort = true;
    
    public static Options options = new Options();

    static {
        options.addOption(OptionBuilder.withLongOpt("remote-address")
                .withArgName("address")
                .hasArg()
                .create('A'));
        options.addOption(OptionBuilder.withLongOpt("remote-port")
                .withArgName("port")
                .hasArg()
                .withType(Number.class)
                .create('P'));
        options.addOption(OptionBuilder.withLongOpt("port")
                .withArgName("port")
                .hasArg()
                .withType(Number.class)
                .create('p'));
    }

    public static void main(String[] args) throws Exception {
        CommandLine cmd = new PosixParser().parse(options, args);

        Peer peer = new Peer(cmd.hasOption("port") ? ((Number) cmd.getParsedOptionValue("port")).intValue() : DEFAULT_PORT);

        if (cmd.hasOption("remote-address")) {
            connectToInitialPeer(peer,
                                 cmd.getOptionValue("remote-address"),
                                 cmd.hasOption("remote-port") ? ((Number) cmd.getParsedOptionValue("remote-port")).intValue() : DEFAULT_PORT);
        }

        System.out.println(" Ready (press Ctrl+C to quit)");
    }

    public static void connectToInitialPeer(Peer peer, String remoteHost, int remotePort) {
        try {
            RemotePeer remotePeer = new RemotePeer(remoteHost, remotePort, peer);
            remotePeer.requestJoin();
            peer.addRemotePeer(remotePeer);
        } catch (IOException e) {
            System.out.println(">>> ERROR --> Could not connect to peer " + remoteHost + ":" + remotePort);
        }
    }

    /**
     * A <i>ServerListener</i> is responsible to handle incoming commands (see {@link ServerCommand}) and perform
     * the corresponding action.
     */
    protected class ServerListener extends Listener {

        protected Peer peer;

        public ServerListener(Peer peer) {
            this.peer = peer;
        }

        @Override
        public void received(Connection connection, Object object) {
            if (object instanceof Command) {
                Command cmd = (Command) object;

                stats.incReceivedCount();

                log("Server received " + cmd.getName() + " from " + connection);

                cmd.perform(connection, peer, remotePeers.get(connection.toString()));

                if (remotePeers.get(connection.toString()) != null) remotePeers.get(connection.toString()).renewTimeoutTimer();
            } else if (!(object instanceof FrameworkMessage)) {
                throw new RuntimeException("Received unknown message of type " + object.getClass().toString());
            }
        }

        @Override
        public void disconnected(Connection connection) {
            RemotePeer remotePeer = remotePeers.get(connection.toString());
            if (remotePeer != null) {
                removeRemotePeer(remotePeer);
            } else {
                peer.log("Closing connection to client " + connection.toString());
            }
        }
    }

    final private  Map<String, RemotePeer> remotePeers = Collections.synchronizedMap(new HashMap<String, RemotePeer>());

    final private int serverPort;

    final protected Server server;

    final private MessageStats stats = new MessageStats();

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Peer Construction & Initialization
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public Peer(int port) throws IOException {
        this.serverPort = port;

        log("--> Starting up " + getAddress());

        this.server = prepareServer();
        this.server.addListener(new ServerListener(this));
        this.server.start();
        this.server.bind(port);

        installShutdownHook();
    }

    protected Server prepareServer() {
        Server server = new Server();
        KryoUtils.registerMessages(server.getKryo());
        return server;
    }

    public String getAddress() {
        try {
            return InetAddress.getLocalHost().getHostAddress() + ":" + serverPort;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public int getPort() {
        return serverPort;
    }

    /**
     * Installing signal handler for graceful shutdown.
     * The strange try-catch construct is necessary to be able to at least try to close all
     * connections. A single try-catch-block would prevent that.
     */
    private void installShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                shutdown();
            }
        });
    }

    public void shutdown() {
        log("Shutting down ...");

        for (RemotePeer peer : new ArrayList<RemotePeer>(remotePeers.values())) {
            try {
                peer.closeConnection();
            } catch (Exception e) {
                System.err.println(e);
            }
        }
        try {
            server.stop();
        } catch (Exception e) {
            System.err.println(e);
        }
        printMessageStats();
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Remote Peer Management
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public Map<String, RemotePeer> getRemotePeers() {
        return remotePeers;
    }

    public synchronized void addRemotePeer(final RemotePeer remotePeer) {
        remotePeers.put(remotePeer.getAddress(), remotePeer);
    }

    public synchronized void removeRemotePeer(final RemotePeer remotePeer) {
        remotePeer.cleanup();

        remotePeers.remove(remotePeer.getAddress());
    }

    public synchronized boolean containsRemotePeer(final RemotePeer remotePeer) {
        return containsRemotePeer(remotePeer.getAddress());
    }

    public synchronized boolean containsRemotePeer(final String address) {
        return remotePeers.containsKey(address);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Message Statistics
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public MessageStats getMessageStats() {
        return stats;
    }

    public void printMessageStats() {
        synchronized (this.getClass()) {
            IOStream.getMsgStatPrintStream().println("");
            if (systemOutWithPort) {
                System.out.println("===== Message Statistics (" + this.serverPort + ") =====");
            } else {
                System.out.println("===== Message Statistics =====");
            }
            IOStream.getMsgStatPrintStream().println("Number of sent messages: " + stats.getSentCount());
            IOStream.getMsgStatPrintStream().println("Number of received messages: " + stats.getReceivedCount());
            IOStream.getMsgStatPrintStream().println("Number of sent bytes: " + stats.getSentBytesCount());
            IOStream.getMsgStatPrintStream().println("");
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Logging
    ///////////////////////////////////////////////////////////////////////////////////////////////
    

    public void log(String message) {
    	
        synchronized (this.getClass()) {
            if (systemOutWithPort) {
                IOStream.getPeerPrintStream().println("--> :" + serverPort + " > " + message);
            } else {
            	IOStream.getPeerPrintStream().println(message);
            }
        }
    }
}