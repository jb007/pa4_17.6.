package ds.peer.commands;

import com.esotericsoftware.kryonet.Connection;
import ds.peer.Command;
import ds.peer.Peer;
import ds.peer.RemotePeer;
import ds.peer.ServerCommand;

public class Ping extends Command<Peer, RemotePeer> {

    @Override
    public String getName() {
        return "PING";
    }

    @Override
    public void perform(Connection connection, Peer peer, RemotePeer remotePeer) {
        // Because of TCP it is sufficient to simply receive the command.
        // No further processing is necessary.
    }
}
