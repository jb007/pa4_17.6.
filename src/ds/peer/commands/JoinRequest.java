package ds.peer.commands;

import com.esotericsoftware.kryonet.Connection;
import ds.peer.Peer;
import ds.peer.RemotePeer;
import ds.peer.ServerCommand;

import java.io.IOException;
import java.util.HashSet;

public class JoinRequest extends ServerCommand<Peer, RemotePeer> {

    @Override
    public String getName() {
        return "JOIN-REQUEST";
    }

    @Override
    public void perform(Connection connection, Peer peer, RemotePeer remotePeer) {
        assert(remotePeer == null);

        try {
            RemotePeer newRemotePeer = new RemotePeer(connection.getRemoteAddressTCP().getAddress().getHostAddress(),
                    serverPort,
                    peer,
                    connection);
            connection.setName(newRemotePeer.getAddress());

            peer.log("Responding with " + peer.getRemotePeers().size() + " known peers");

            sendResponse(connection, peer, new JoinResponse(new HashSet<RemotePeer>(peer.getRemotePeers().values())));

            peer.addRemotePeer(newRemotePeer);
        } catch (IOException e) {
            System.out.println(">>> ERROR --> Could not connect to peer " + connection.getRemoteAddressTCP().getAddress().getHostAddress() + ":" + serverPort);
        }
    }
}
