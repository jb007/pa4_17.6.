package ds.peer.commands;

import com.esotericsoftware.kryonet.Connection;
import ds.peer.ClientCommand;
import ds.peer.Peer;
import ds.peer.RemotePeer;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;

public class JoinResponse extends ClientCommand<Peer, RemotePeer> {

    private String[] knownPeers;

    /**
     * The constructor with no arguments is needed by the serializer.
     */
    public JoinResponse() { }

    /**
     * The constructor with arguments is used by the server to prepare the response with the
     * requested data.
     */
    public JoinResponse(Collection<RemotePeer> knownPeers) {
        Collection<String> peerAddresses = new HashSet<String>();
        for (RemotePeer peer : knownPeers) {
            peerAddresses.add(peer.getAddress());
        }
        this.knownPeers = peerAddresses.toArray(new String[peerAddresses.size()]);
    }

    public String getName() {
        return "JOIN-RESPONSE";
    }

    @Override
    public void perform(Connection connection, Peer peer, RemotePeer remotePeer) {
        for (String knownPeerAddress : knownPeers) {
            String host = knownPeerAddress.substring(0, knownPeerAddress.lastIndexOf(":"));
            int port = Integer.parseInt(knownPeerAddress.substring(knownPeerAddress.lastIndexOf(":") + 1));

            try {
                if (!knownPeerAddress.equals(peer.getAddress()) && !peer.containsRemotePeer(knownPeerAddress)) {
                    RemotePeer knownPeer = new RemotePeer(host, port, peer);
                    peer.addRemotePeer(knownPeer);
                    knownPeer.requestJoin();
                }
            } catch (IOException e) {
                System.out.println(">>> ERROR --> Could not connect to peer " + knownPeerAddress);
            }
        }
    }
}
