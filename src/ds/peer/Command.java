package ds.peer;

import com.esotericsoftware.kryonet.Connection;

/**
 * A <i>Command</i> acts as a message between connection and server.
 * The Command-Pattern is used because every single message triggers some action at the receiver-
 * side and exactly that action is encapsulated and performed by a <i>Command</i>.
 * Because of the used network communication library all fields have to be public to avoid the
 * need for a specific and self-implemented serialization mechanism.
 */
public abstract class Command<P extends Peer, R extends RemotePeer> {

    public int serverPort;

    abstract public String getName();

    abstract public void perform(Connection connection, P peer, R remotePeer);

}
