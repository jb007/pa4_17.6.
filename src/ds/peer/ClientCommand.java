package ds.peer;

/**
 * A <i>ClientCommand</i> is a {@link Command} which is executed by the connection.
 * Typically a <i>ClientCommand</i> is the response to a previously send {@link ServerCommand}.
 * The server prepares the command with the requested information, if any, and sends it to the
 * connection who then performs an action to process the response.
 */
public abstract class ClientCommand<P extends Peer, R extends RemotePeer> extends Command<P, R> {
}
