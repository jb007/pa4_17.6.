package ds.peer;

import java.io.PrintStream;

public class IOStream {
	
	private static PrintStream peerOut = System.out;
	private static PrintStream msgStatOut = System.out;
	private static PrintStream testOut = System.out;
	
	public static PrintStream getPeerPrintStream() {
		return peerOut;
	}
	
	public static void setPeerPrintStream(PrintStream out) throws Exception {
		IOStream.peerOut = out;
	}
	
	public static PrintStream getMsgStatPrintStream() {
		return msgStatOut;
	}
	
	public static void setMsgStatPrintStream(PrintStream out) throws Exception {
		IOStream.msgStatOut = out;
	}
	
	public static PrintStream getTestPrintStream() {
		return testOut;
	}
	
	public static void setTestPrintStream(PrintStream out) throws Exception {
		IOStream.testOut = out;
	}

}
