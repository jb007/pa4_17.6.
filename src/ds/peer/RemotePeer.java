package ds.peer;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.kryonet.Listener;
import ds.peer.commands.JoinRequest;
import ds.peer.commands.JoinResponse;
import ds.peer.commands.Ping;
import ds.utils.KryoUtils;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class RemotePeer {

    /**
     * A <i>ClientListener</i> is responsible to handle server responses (see {@link ClientCommand}) and perform
     * corresponding actions.
     */
    private class ClientListener extends Listener {

        private RemotePeer remotePeer;

        public ClientListener(RemotePeer remotePeer) {
            this.remotePeer = remotePeer;
        }

        @Override
        public void received(Connection connection, Object object) {
            if (object instanceof Command) {
                Command cmd = (Command) object;

                peer.log("Client received " + cmd.getName() + " from " + remotePeer.getAddress());

                cmd.perform(connection, peer, remotePeer);

                remotePeer.renewTimeoutTimer();
            } else if (!(object instanceof FrameworkMessage)) {
                throw new RuntimeException("Received unknown message of type " + object.getClass().toString());
            }
        }

        @Override
        public void disconnected(Connection connection) {
            remotePeer.cleanup();
        }
    }

    final Peer peer;

    final protected Connection connection;

    final String address;

    final MessageStats stats;

    public static final int TIMEOUT = 10000;

    Timer timeoutTimer;

    public RemotePeer(final String host, final int port, final Peer peer) throws IOException {
        this(host, port, peer, null);
    }

    public RemotePeer(final String host, final int port, final Peer peer, final Connection conn) throws IOException {
        this.address = host + ":" + port;
        this.peer = peer;
        this.stats = peer.getMessageStats();

        if (conn != null) {
            this.connection = conn;
        } else {
            connection = prepareClient();
            connection.addListener(new ClientListener(this));
            ((Client ) connection).start();
            ((Client ) connection).connect(TIMEOUT, host, port);
        }

        renewTimeoutTimer();
    }

    private Client prepareClient() {
        Client client = new Client();
        KryoUtils.registerMessages(client.getKryo());
        return client;
    }

    public String getAddress() {
        return this.address;
    }

    public synchronized void closeConnection() {
        if (connection.isConnected()) {
            peer.log("Closing connection to " + getAddress());

            connection.close();
        }
    }

    public synchronized void cleanup() {
        peer.log("Cleaning up connection to " + getAddress());

        if (connection instanceof Client) ((Client ) connection).stop();

        timeoutTimer.cancel();
    }

    public void requestJoin() {
        if (connection.isConnected()) {
            sendCommand(new JoinRequest());
        } else {
            final RemotePeer thisRemotePeer = this;
            connection.addListener(new Listener() {
                public void connected(Connection connection) {
                    sendCommand(new JoinRequest());
                }
            });
        }
    }

    public void ping() {
        sendCommand(new Ping());
    }

    protected synchronized void sendCommand(Command command) {
        if (connection.isConnected()) {
            peer.log("Sending " + command.getName() + " to " + address);

            command.serverPort = peer.getPort();

            stats.incSentBytesBy(connection.sendTCP(command));
            stats.incSentCount();

            renewTimeoutTimer();
        } else {
            peer.log("Could not send " + command.getName() + ".");
            peer.log("Removing " + this.getAddress() + " because the connection has been closed!");
            peer.removeRemotePeer(this);
        }
    }

    public synchronized void renewTimeoutTimer() {
        // Ensuring that connection is connected
        if (!connection.isConnected()) {
            return;
        }

        if (timeoutTimer != null) {
            timeoutTimer.cancel();
        }

        final RemotePeer thisRemotePeer = this;
        timeoutTimer = new Timer();
        timeoutTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                thisRemotePeer.ping();
            }
        }, TIMEOUT);
    }
}
