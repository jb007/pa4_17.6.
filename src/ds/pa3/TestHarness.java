package ds.pa3;



import ds.peer.IOStream;
import ds.peer.Peer;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class TestHarness {

    public static void main(String[] args) throws Exception {
        TestHarness testHarness = new TestHarness();

//        IOStream.setPeerPrintStream(new PrintStream(new FileOutputStream("export/test1.txt"))); 
//        IOStream.setTestPrintStream(new PrintStream(new FileOutputStream("export/test1.txt")));
//        testHarness.runBasicTestOne(4);
//        System.out.println("");
        
//        IOStream.setPeerPrintStream(new PrintStream(new FileOutputStream("export/test2.txt")));
//        testHarness.runBasicTestTwo();
//        System.out.println("");
        
        IOStream.setPeerPrintStream(new PrintStream(new FileOutputStream("export/test3.txt")));
        testHarness.runBasicTestThree(0,10);
        System.out.println("");
        
//        IOStream.setPeerPrintStream(new PrintStream(new FileOutputStream("export/test4.txt")));
//        testHarness.runBasicTestFour();
//        System.out.println("");

        
//        IOStream.setPeerPrintStream(new PrintStream(new FileOutputStream("export/experiment1.txt")));
//        testHarness.runExperiment1Case1();
//        System.out.println("");
//        testHarness.runExperiment1Case2();
//        System.out.println("");
        
        testHarness.runExperiment2();
        //falls aus build Verzeichnis aus gestartet, sonst "export/experiments.xls"
        String fileName="export/experiments.xls";
        ExcelFileOut.writeDataToExcelFile(fileName, testHarness.excelData, 20000);
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	public static final int MAX_NUM_OF_MACHINES = 8;
	public static final int MIN_NUM_OF_MACHINES = 4;
	public static final int NUM_MSGSTAT_INFOS = 3;
	
	public static final long TEST_PERIOD = 10000; //miliseconds

    private static final int RANDOM_STRING_LENGTH = 100;

    private List<Peer> peerList;
    
    
    /**
     * 1. row: number of machines in system = 4
     * 2. row: number of machines in system = 5
     * 3. row: number of machines in system = 6
     * 4. row: number of machines in system = 7
     * 5. row: number of machines in system = 8
     * 
     * 1. column: peer1 - number of sent messages
     * 2. column: peer1 - number of received messages
     * 3. column: peer1 - number of sent bytes
     * 4. column: peer2 - number of sent messages
     * 5. column: peer2 - number of received messages
     * 6. column: peer2 - number of sent bytes
     * etc.
     */
    private long[][] excelData;

    public TestHarness() {
        excelData = new long[MAX_NUM_OF_MACHINES - MIN_NUM_OF_MACHINES + 1][MAX_NUM_OF_MACHINES * NUM_MSGSTAT_INFOS];
	}

    public void setup(int n) throws Exception {
    	log("--> Initializing " + n + " Peers ...");
        
        peerList = new ArrayList<Peer>();
        int initPort = 2221;

        for (int i = 0; i<n; i++) {
        	peerList.add(new Peer(initPort+i));
        	if (i>0) {
        		Peer.connectToInitialPeer(peerList.get(i), "127.0.0.1", initPort);
        	}
        }
    }
    
    public void shutdown() {
    	for (Peer p : peerList) {
    		p.shutdown();
    	}
    }

    
    
    //==========================================================================
    //===============================  Tests  ==================================
    //==========================================================================
    
    //test failure detection
    public void runBasicTestOne(int numPeers) throws Exception {
        
        setup(numPeers);
        
        log("Running first basic test - " + (TEST_PERIOD*(numPeers+1)/1000) + "s remaining...");
        while (System.currentTimeMillis() < TEST_PERIOD) {} //busy waiting
        
        for (int i = 0; i < numPeers; i++) { 
//	        log("Disabling peer " + peerList.get(i).getServerPort() + " for " + (TEST_PERIOD/1000) + "s");
	        disablePeerResponse(peerList.get(i), TEST_PERIOD);
//	        log("Peer " + peerList.get(i).getServerPort() + " now enabled.");
        }
                
        log("Running first basic test ... [DONE]");
       
        shutdown();
        
    }

    public void disablePeerResponse(Peer peer, long period) throws Exception {
//    	long endDisabling = System.currentTimeMillis() + period;
////    	peer.getServer().removeListener(peer.getServerListener());	//disable ServerListener
////    	peer.removeServerListener();
//    	peer.setLossActivated(true);
//    	while (System.currentTimeMillis() < endDisabling) {} //busy waiting
////    	peer.getServer().addListener(peer.getServerListener());	//enable ServerListener
//    	peer.setLossActivated(false);
//    	//send new JoinRequest?
    }
    
    //test satisfaction of time-bounded completeness
    //time-bounded completeness = any failure should be reported by at least one 
    //							  non-faulty machine within 10 seconds
    public void runBasicTestTwo() throws Exception {

    }
    
    //test support of different numbers of machines
    public void runBasicTestThree(int minNumMachines, int maxNumMachines) throws Exception {
    	if (minNumMachines <= maxNumMachines) {
    		
    		log("========================= Test 3: running " + maxNumMachines + " machines =========================");
	        
	        int i = 0;
	        int numMachines = minNumMachines;
	        peerList = new ArrayList<Peer>();
	        int initPort = 2221;
	        long time = System.currentTimeMillis();
	        
	        log("Test 3: " + numMachines + " machines running");       
	        peerList.add(new Peer(initPort+i));
	        
	        while (numMachines < maxNumMachines) {
	            if (System.currentTimeMillis() >= time+2000) {
	            	i++;
	        		peerList.add(new Peer(initPort+i));
	            	if (i>0) {
	            		Peer.connectToInitialPeer(peerList.get(i), "127.0.0.1", initPort);
	            	}
	            	numMachines++;
	        		time+=2000;
	        		log("Test 3: " + numMachines + " machines running");
	        	}
	        }
	        
	        shutdown();      
	        
	        log("\n=================== Test 3: running " + maxNumMachines + " machines ... [DONE] ====================");
	        log("\n\n\n");  
    	} else {
    		log("Max number of machines smaller than min number");
    	}
    }
    
    //test detection of 2 simultaneous failures
    public void runBasicTestFour() throws Exception {

    }
    
    
    
    
    //==========================================================================
    //==========================  Experiments  =================================
    //==========================================================================

    //experiment 1, scenario 1 - false positives at 4 machines with no message loss
    //false positive rate = average number of reported failures per second, 
    //						when there are in fact no failures
    public void runExperiment1Case1() throws Exception {
    	//0
    }

    //experiment 1, scenario 2 - false positives at 4 machines with 
    //							 50% message loss rate at one machine
    public void runExperiment1Case2() throws Exception {

    }

    //experiment 2 - system data rate (number of messages per second and 
    //				 bytes per second) transmitted when there are no failures
    //				 versus the number of machines (n=4 to 8)
    public void runExperiment2() throws Exception {
    	for (int n = MIN_NUM_OF_MACHINES; n <= (MAX_NUM_OF_MACHINES); n++) {
        	runMachines(n);
        	//writing excel data
        	for (int j = 0; j < n; j++) {
        		excelData[n-MIN_NUM_OF_MACHINES][j*NUM_MSGSTAT_INFOS] = peerList.get(j).getMessageStats().getSentCount();
        		excelData[n-MIN_NUM_OF_MACHINES][j*NUM_MSGSTAT_INFOS+1] = peerList.get(j).getMessageStats().getReceivedCount();
        		excelData[n-MIN_NUM_OF_MACHINES][j*NUM_MSGSTAT_INFOS+2] = peerList.get(j).getMessageStats().getSentBytesCount();
        	}
        }
    }
    
    
    private void runMachines(int n) throws Exception {
    	log("========================= Experiment 2 - running " + n + " machines =========================");
        
        setup(n);
        
        log("Experiment 2 - running " + n + " machines. " + (TEST_PERIOD/1000) + "s remaining...");

        long time = System.currentTimeMillis();
        long stop = System.currentTimeMillis() + TEST_PERIOD;
        
        while (System.currentTimeMillis() <= stop) {

        	if (System.currentTimeMillis() >= time+2000) {
        		time+=2000;
        		log("Experiment 2 - running " + n + " machines. " +  (stop-time)/1000 + "s remaining...");
        	}
        }
        
        shutdown();      
        
        log("=================== Experiment 2 - running " + n + " machines ... [DONE] ====================");
        log("\n\n\n");       
    }
    
    
    public void log(String message) {
    	synchronized (this.getClass()) {
            IOStream.getTestPrintStream().println(message);
        }
    }

}
