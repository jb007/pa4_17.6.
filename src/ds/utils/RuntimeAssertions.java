package ds.utils;

public class RuntimeAssertions {

    public static void assertTrue(boolean assertion) {
        if (!assertion) throw new RuntimeException();
    }

    private RuntimeAssertions() {}
}
