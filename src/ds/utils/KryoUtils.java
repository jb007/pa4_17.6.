package ds.utils;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.rmi.ObjectSpace;
import ds.dc.commands.*;
import ds.data.Node;
import ds.peer.commands.JoinRequest;
import ds.peer.commands.JoinResponse;
import ds.peer.commands.Ping;

public class KryoUtils {

    public static void registerMessages(Kryo kryo) {
        ObjectSpace.registerClasses(kryo);
        kryo.register(JoinRequest.class);
        kryo.register(JoinResponse.class);
        kryo.register(Ping.class);

        kryo.register(DCJoinRequest.class);
        kryo.register(DCJoinResponse.class);
        kryo.register(InsertDataCmd.class);
        kryo.register(DeleteDataCmd.class);
        kryo.register(NodeDataRequest.class);
        kryo.register(ReplicatedNodeAnnouncement.class);

        kryo.register(String[].class);
        kryo.register(Integer[].class);
        kryo.register(java.util.HashSet.class);
        kryo.register(Node.class);
    }
}
