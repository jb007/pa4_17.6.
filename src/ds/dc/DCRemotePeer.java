package ds.dc;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.rmi.ObjectSpace;
import ds.data.Node;
import ds.data.ReplicatedNode;
import ds.dc.commands.DCJoinRequest;
import ds.dc.commands.ReplicatedNodeAnnouncement;
import ds.peer.RemotePeer;

import java.io.IOException;

public class DCRemotePeer extends RemotePeer {

    public DCRemotePeer(final String host, final int port, final DCPeer peer) throws IOException {
        this(host, port, peer, null);
    }

    public DCRemotePeer(final String host, final int port, final DCPeer peer, final Connection conn) throws IOException {
        super(host, port, peer, conn);

        connection.addListener(new Listener() {
            @Override
            public void disconnected(Connection connection) {
                for(ReplicatedNode node : peer.getRootNode().getReplicationNodes()) {
                    node.removeNodeBelongingToConnection(connection);
                }
            }
        });
    }

    public Node getNode(int id) {
        return ObjectSpace.getRemoteObject(connection, id, Node.class);
    }

    public void requestJoin() {
        if (connection.isConnected()) {
            sendCommand(new DCJoinRequest());
        } else {
            final RemotePeer thisRemotePeer = this;
            connection.addListener(new Listener() {
                public void connected(Connection connection) {
                    sendCommand(new DCJoinRequest());
                }
            });
        }
    }

    public void announceReplicatedNode(Integer nodeID) {
        sendCommand(new ReplicatedNodeAnnouncement(nodeID));
    }
}
