package ds.dc.commands;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.rmi.ObjectSpace;
import ds.data.LeafNode;
import ds.data.Node;
import ds.dc.DCPeer;
import ds.dc.DCRemotePeer;
import ds.peer.Command;
import ds.peer.ServerCommand;

public class NodeDataRequest extends ServerCommand<DCPeer, DCRemotePeer> {

    private int nodeID;

    public NodeDataRequest() {
    }

    public NodeDataRequest(int nodeID) {
        this.nodeID = nodeID;
    }

    @Override
    public String getName() {
        return "NODE-DATA-REQUEST";
    }

    @Override
    public void perform(final Connection connection, final DCPeer peer, final DCRemotePeer remotePeer) {
        final Node node = peer.getLocalNode(nodeID);

        if (node  == null || !(node instanceof LeafNode)) {
            peer.log("  ERROR: Cannot provide data for node " + nodeID);
            return;
        }

        new Thread() {
            public void run() {
                peer.getReplicatedNode(nodeID).addNode(ObjectSpace.getRemoteObject(connection, nodeID, Node.class));
                for (Long key : node.getKeys()) {
                    sendResponse(connection, peer, new InsertDataCmd(key, node.lookup(key)));
                }
            }
        }.start();
    }
}
