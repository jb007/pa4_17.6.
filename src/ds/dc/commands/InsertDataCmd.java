package ds.dc.commands;

import com.esotericsoftware.kryonet.Connection;
import ds.dc.DCPeer;
import ds.dc.DCRemotePeer;
import ds.peer.Command;

public class InsertDataCmd extends Command<DCPeer, DCRemotePeer> {

    private long key;

    private String value;

    public InsertDataCmd() { }

    public InsertDataCmd(long key, String value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String getName() {
        return "INSERT-DATA-CMD";
    }

    @Override
    public void perform(Connection connection, DCPeer peer, DCRemotePeer remotePeer) {
        // Ignore message if it does not apply to this peer (any more).
        // This can happen for example if this node sends a message to all known replications which again notify all replications they know
        // and this also includes the one on the initial peer.

        if (peer.getRootNode().lookup(key) == null || !peer.getRootNode().lookup(key).equals(value)) {
            peer.getRootNode().insert(key, value);
        }
    }
}
