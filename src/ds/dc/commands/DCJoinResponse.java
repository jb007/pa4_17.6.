package ds.dc.commands;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.rmi.ObjectSpace;
import ds.data.Node;
import ds.data.NodeBase;
import ds.data.ReplicatedNode;
import ds.dc.DCPeer;
import ds.dc.DCRemotePeer;
import ds.peer.ClientCommand;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class DCJoinResponse extends ClientCommand<DCPeer, DCRemotePeer> {

    private Integer[] nodeIDsToBeReplicated;

    private Integer[] replicatedNodeIDs;

    public DCJoinResponse() { }

    public DCJoinResponse(Collection<Integer> nodeIDsToBeReplicated) {
        this.nodeIDsToBeReplicated = nodeIDsToBeReplicated.toArray(new Integer[nodeIDsToBeReplicated.size()]);
    }

    public String getName() {
        return "JOIN-RESPONSE";
    }

    @Override
    public void perform(final Connection connection, final DCPeer peer, final DCRemotePeer remotePeer) {
        new Thread() {
            public void run() {
                peer.log("PROCESSING JOIN-RESPONSE WITH " + nodeIDsToBeReplicated.length + " NODES TO BE REPLICATED");

                for (Integer nodeID : nodeIDsToBeReplicated) {
                    peer.startReplicatingOrIgnore(nodeID, connection);

                    if (NodeBase.levelFromNumericID(nodeID) == DCPeer.DATA_LEVEL) {
                        connection.sendTCP(new NodeDataRequest(nodeID));
                    }

                    peer.announceReplicatedNode(nodeID);
//                    peer.log("STARTED REPLICATING NODE " + nodeID);
                }
            }
        }.start();
    }
}
