package ds.dc.commands;

import com.esotericsoftware.kryonet.Connection;
import ds.dc.DCPeer;
import ds.dc.DCRemotePeer;
import ds.peer.Command;

public class DeleteDataCmd extends Command<DCPeer, DCRemotePeer> {

    private long key;

    public DeleteDataCmd() { }

    public DeleteDataCmd(long key) {
        this.key = key;
    }

    @Override
    public String getName() {
        return "DELETE-DATA-CMD";
    }

    @Override
    public void perform(Connection connection, DCPeer peer, DCRemotePeer remotePeer) {
        // Ignore message if it does not apply to this peer (any more).
        // This can happen for example if this node sends a message to all known replications which again notify all replications they know
        // and this also includes the one on the initial peer.

        if (peer.getRootNode().lookup(key) != null) {
            peer.getRootNode().delete(key);
        }
    }
}
