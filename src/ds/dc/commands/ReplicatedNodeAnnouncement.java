package ds.dc.commands;

import com.esotericsoftware.kryonet.Connection;
import ds.dc.DCPeer;
import ds.dc.DCRemotePeer;
import ds.peer.Command;

public class ReplicatedNodeAnnouncement extends Command<DCPeer, DCRemotePeer> {

    private int nodeID;

    public ReplicatedNodeAnnouncement() { }

    public ReplicatedNodeAnnouncement(int nodeID) {
        this.nodeID = nodeID;
    }

    @Override
    public String getName() {
        return "REPLICATED-NODE-ANNOUNCEMENT";
    }

    @Override
    public void perform(final Connection connection, final DCPeer peer, final DCRemotePeer remotePeer) {
        // Ignoring node if it is not relevant for this peer.
        if (peer.getReplicatedNode(nodeID) != null) {
            peer.getReplicatedNode(nodeID).addNode(remotePeer.getNode(nodeID));
        }
    }
}
