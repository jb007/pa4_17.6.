package ds.dc.commands;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.rmi.ObjectSpace;
import ds.data.InnerNode;
import ds.data.LeafNode;
import ds.data.Node;
import ds.data.ReplicatedNode;
import ds.dc.DCPeer;
import ds.dc.DCRemotePeer;
import ds.peer.ServerCommand;

import java.io.IOException;
import java.util.*;

public class DCJoinRequest extends ServerCommand<DCPeer, DCRemotePeer> {

    private static final int MIN_REPLICATION_COUNT = 3;

    private Integer[] replicatedNodeIDs = {};

    public DCJoinRequest() { }

    public DCJoinRequest(Integer[] replicatedNodeIDs) {
        this.replicatedNodeIDs = replicatedNodeIDs;
    }

    @Override
    public String getName() {
        return "JOIN-REQUEST";
    }

    @Override
    public void perform(final Connection connection, final DCPeer peer, final DCRemotePeer remotePeer) {
        assert(remotePeer == null);

        try {
            final DCRemotePeer newRemotePeer = new DCRemotePeer(connection.getRemoteAddressTCP().getAddress().getHostAddress(),
                    serverPort,
                    peer,
                    connection);
            connection.setName(newRemotePeer.getAddress());

            for (Integer nodeID : replicatedNodeIDs) {
                ReplicatedNode replicatedNode = peer.getReplicatedNode(nodeID);
                if (replicatedNode != null && replicatedNode.hasLocalNode()) {
                    replicatedNode.addNode(newRemotePeer.getNode(nodeID));
                }
            }

            Set<Integer> nodeIDsToBeReplicated = new HashSet<Integer >();
            for (Node node : peer.getRootNode().getLocalNodes()) {
                if (peer.getReplicatedNode(node.getNumericID()).getReplicationCount() >= MIN_REPLICATION_COUNT) continue;
                if (node instanceof InnerNode && !((InnerNode) node).hasBothLocalChildren()) continue;

                nodeIDsToBeReplicated.add(node.getNumericID());
            }

            sendResponse(connection, peer, new DCJoinResponse(nodeIDsToBeReplicated));

            peer.addRemotePeer(newRemotePeer);
        } catch (IOException e) {
            System.out.println(">>> ERROR --> Could not connect to peer " + connection.getRemoteAddressTCP().getAddress().getHostAddress() + ":" + serverPort);
        }
    }
}
