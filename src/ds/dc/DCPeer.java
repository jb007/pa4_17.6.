package ds.dc;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.esotericsoftware.kryonet.rmi.ObjectSpace;
import ds.data.*;
import ds.dc.commands.NodeDataRequest;
import ds.peer.Peer;
import ds.peer.RemotePeer;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.PosixParser;

import java.io.IOException;

import static ds.utils.RuntimeAssertions.*;

public class DCPeer extends Peer {

    public static void main(String[] args) throws Exception {
        CommandLine cmd = new PosixParser().parse(options, args);

        DCPeer peer = new DCPeer(cmd.hasOption("port") ? ((Number) cmd.getParsedOptionValue("port")).intValue() : DEFAULT_PORT);

        if (cmd.hasOption("remote-address")) {
            connectToInitialPeer(peer,
                                 cmd.getOptionValue("remote-address"),
                                 cmd.hasOption("remote-port") ? ((Number) cmd.getParsedOptionValue("remote-port")).intValue() : DEFAULT_PORT);
        } else {
            peer.createFullSearchTree();
        }

        System.out.println("--> Ready (press Ctrl+C to quit)");
    }

    public static void connectToInitialPeer(DCPeer peer, String remoteHost, int remotePort) {
        try {
            DCRemotePeer remotePeer = new DCRemotePeer(remoteHost, remotePort, peer);
            remotePeer.requestJoin();
            peer.addRemotePeer(remotePeer);

            peer.rootNode = new ReplicatedNode(0, 0, new InnerNode(0, 0,
                    new ReplicatedNode(1, 0, remotePeer.getNode(256)),
                    new ReplicatedNode(1, 1, remotePeer.getNode(257))));
            peer.registerLocalNodeForRemoteUse(peer.rootNode);
        } catch (IOException e) {
            System.out.println(">>> ERROR --> Could not connect to peer " + remoteHost + ":" + remotePort);
        }
    }

    private ObjectSpace objectSpace;

    public void announceReplicatedNode(Integer nodeID) {
        for (RemotePeer remotePeer : this.getRemotePeers().values()) {
            ((DCRemotePeer )remotePeer).announceReplicatedNode(nodeID);
        }
    }

    protected class ServerListener extends Listener {

        private DCPeer peer;

        public ServerListener(DCPeer peer) {
            this.peer = peer;
        }

        @Override
        public void connected(Connection connection) {
            connection.setName(connection.getRemoteAddressTCP().toString().substring(1));

            peer.objectSpace.addConnection(connection);
        }

        @Override
        public void disconnected(Connection connection) {
            peer.objectSpace.removeConnection(connection);

            for(ReplicatedNode node : rootNode.getReplicationNodes()) {
                node.removeNodeBelongingToConnection(connection);
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Peer Construction & Initialization
    ///////////////////////////////////////////////////////////////////////////////////////////////

    public DCPeer(int port) throws IOException {
        super(port);

        objectSpace = new ObjectSpace();

        server.addListener(new ServerListener(this));
    }

    private Node rootNode;

    public Node getRootNode() {
        return rootNode;
    }

    public void createFullSearchTree() {
        if (rootNode != null) {
            objectSpace.remove(rootNode);
        }

        rootNode =
            new ReplicatedNode(0, 0, new InnerNode(0, 0,
                new ReplicatedNode(1, 0, new InnerNode(1, 0,
                    new ReplicatedNode(2, 0, new InnerNode(2, 0,
                        new ReplicatedNode(3, 0, new InnerNode(3, 0,
                            new ReplicatedNode(4, 0, new LeafNode(4, 0)),
                            new ReplicatedNode(4, 8, new LeafNode(4, 8)))),
                        new ReplicatedNode(3, 4, new InnerNode(3, 4,
                            new ReplicatedNode(4, 4, new LeafNode(4, 4)),
                            new ReplicatedNode(4, 12, new LeafNode(4, 12)))))),
                    new ReplicatedNode(2, 2, new InnerNode(2, 2,
                        new ReplicatedNode(3, 2, new InnerNode(3, 2,
                            new ReplicatedNode(4, 2, new LeafNode(4, 2)),
                            new ReplicatedNode(4, 10, new LeafNode(4, 10)))),
                        new ReplicatedNode(3, 6, new InnerNode(3, 6,
                            new ReplicatedNode(4, 6, new LeafNode(4, 6)),
                            new ReplicatedNode(4, 14, new LeafNode(4, 14)))))))),
                new ReplicatedNode(1, 1, new InnerNode(1, 1,
                    new ReplicatedNode(2, 1, new InnerNode(2, 1,
                        new ReplicatedNode(3, 1, new InnerNode(3, 1,
                            new ReplicatedNode(4, 1, new LeafNode(4, 1)),
                            new ReplicatedNode(4, 9, new LeafNode(4, 9)))),
                        new ReplicatedNode(3, 5, new InnerNode(3, 5,
                            new ReplicatedNode(4, 5, new LeafNode(4, 5)),
                            new ReplicatedNode(4, 13, new LeafNode(4, 13)))))),
                    new ReplicatedNode(2, 3, new InnerNode(2, 3,
                        new ReplicatedNode(3, 3, new InnerNode(3, 3,
                            new ReplicatedNode(4, 3, new LeafNode(4, 3)),
                            new ReplicatedNode(4, 11, new LeafNode(4, 11)))),
                        new ReplicatedNode(3, 7, new InnerNode(3, 7,
                            new ReplicatedNode(4, 7, new LeafNode(4, 7)),
                            new ReplicatedNode(4, 15, new LeafNode(4, 15))))))))));

        for (Node node : rootNode.getLocalNodes()) {
            registerLocalNodeForRemoteUse(node);
        }
    }

    private void registerLocalNodeForRemoteUse(Node node) {
//        this.log("REGISTERING NODE " + node.getNumericID());
        this.objectSpace.register(this.rootNode.getNumericID(), this.rootNode);
    }

    public static final int DATA_LEVEL = 4;

    public void startReplicatingOrIgnore(Integer nodeID, Connection connection) {
        // Root-node is always replicated. Therefore ignore it here.
        // Already replicated nodes are also ignored here.
        if (nodeID.intValue() == 0 || this.getLocalNode(nodeID) != null) {
            return;
        }

        // Ensure parent node is already replicated
        this.startReplicatingOrIgnore(NodeBase.getParentNumericIDForID(nodeID), connection);

//        this.log("STARTING REPLICATION FOR NODE " + nodeID + "(LEVEL: " + nodeLevel + ", KEY: " + nodeKey + ")");

        Node newNode = createLocalNode(nodeID, connection);
        this.getReplicatedNode(nodeID).addNode(newNode);
        this.registerLocalNodeForRemoteUse(newNode.getLocalNodes().iterator().next());
    }

    private Node createLocalNode(Integer nodeID, Connection connection) {
        Node localNode;

        int nodeLevel = NodeBase.levelFromNumericID(nodeID);
        int nodeKey = NodeBase.keyFromNumericID(nodeID);

        if (nodeLevel == DATA_LEVEL) {
            // If node belongs to data level create a leaf node.
            localNode = new LeafNode(nodeLevel, nodeKey);
        } else {
            // If node does not belong to data level create an inner node and add the connection as target for the newly created child nodes.
            ReplicatedNode left = new ReplicatedNode(nodeLevel + 1, nodeKey);
            left.addNode(ObjectSpace.getRemoteObject(connection, left.getNumericID(), Node.class));
            assertTrue(left.getParentNumericID().intValue() == nodeID);
//            this.log("CREATING " + (nodeLevel + 1) + ":" + nodeKey);

            ReplicatedNode right = new ReplicatedNode(nodeLevel + 1, nodeKey | (1 << nodeLevel));
            right.addNode(ObjectSpace.getRemoteObject(connection, right.getNumericID(), Node.class));
            assertTrue(left.getParentNumericID().intValue() == nodeID);
//            this.log("CREATING " + (nodeLevel + 1) + ":" + (nodeKey | (1 << nodeLevel)));

            localNode = new InnerNode(nodeLevel, nodeKey, left, right);
        }
        return localNode;
    }

    public synchronized ReplicatedNode getReplicatedNode(Integer nodeID) {
        for(ReplicatedNode node : rootNode.getReplicationNodes()) {
            if (node.getNumericID() == nodeID) {
                return node;
            }
        }
        return null;
    }

    public synchronized Node getLocalNode(Integer nodeID) {
        for(Node node : rootNode.getLocalNodes()) {
            if (node.getNumericID() == nodeID.intValue()) {
                return node;
            }
        }
        return null;
    }
}
