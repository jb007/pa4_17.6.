package ds.client;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.rmi.ObjectSpace;
import ds.data.Node;
import ds.peer.commands.JoinRequest;
import ds.peer.commands.JoinResponse;
import ds.peer.commands.Ping;
import ds.utils.KryoUtils;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class MyDC {

    private class ClientListener extends Listener {

        MyDC myDC;

        public ClientListener(MyDC myDC) {
            this.myDC = myDC;
        }

        @Override
        public void connected(Connection connection) {
            rootNode = ObjectSpace.getRemoteObject(connection, 0, Node.class);
        }
    }

    private final Client client;

    private Node rootNode;

    public static final int TIMEOUT = 10000;

    private MessageDigest md5;

    public MyDC(final String host, final int port) throws IOException {
        client = new Client();
        client.addListener(new ClientListener(this));
        KryoUtils.registerMessages(client.getKryo());
        client.start();
        client.connect(TIMEOUT, host, port);

        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public void shutdown() {
        client.close();
        client.stop();
    }

    public boolean isReady() {
        return rootNode != null;
    }

    public void insert(String key, String value) {
        if (!isReady()) {
            throw new RuntimeException();
        }

        rootNode.insert(generateNumericKey(key), value);
    }

    public String lookup(String key) {
        if (!isReady()) {
            throw new RuntimeException();
        }

        return rootNode.lookup(generateNumericKey(key));
    }

    public void delete(String key) {
        if (!isReady()) {
            throw new RuntimeException();
        }

        rootNode.delete(generateNumericKey(key));
    }

    private long generateNumericKey(String key) {
        byte[] hash = md5.digest(key.getBytes());

        long intKey = (hash[12] + 128);
        intKey += (hash[13] + 128) * Math.pow(2, 8);
        intKey += (hash[14] + 128) * Math.pow(2, 16);
        intKey += (hash[15] + 128) * Math.pow(2, 24);

        return intKey;
    }
}
