package ds.example;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;

import asg.cliche.Command;
import asg.cliche.ShellFactory;
import asg.cliche.ShellManageable;

import ds.client.MyDC;

public class Wikipedia implements ShellManageable {

	private MyDC myDC;    
	
	public static Options options = new Options();

    static {
        options.addOption(OptionBuilder.withLongOpt("remote-address")
                .withArgName("address")
                .hasArg()
                .isRequired()
                .create('A'));
        options.addOption(OptionBuilder.withLongOpt("remote-port")
                .withArgName("port")
                .hasArg()
                .withType(Number.class)
                .isRequired()
                .create('P'));
    }


    public Wikipedia(MyDC myDC) {
    	this.myDC = myDC;
	}

	public static void main(String[] args) throws Exception {
        CommandLine cmd = new PosixParser().parse(options, args);

        String host = cmd.getOptionValue("remote-address");
        int port = ((Number) cmd.getParsedOptionValue("remote-port")).intValue();

        MyDC myDC = new MyDC(host, port);
        
        ShellFactory.createConsoleShell("Wikipedia", "", new Wikipedia(myDC))
        .commandLoop(); // and three.
    }
	
	public String readWikipedia() {
    	String wiki = "";
    	
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(new File("import/testwikishort")));
//        	br = new BufferedReader(new FileReader(new File("import/wikipedia")));
//        	br = new BufferedReader(new FileReader(new File("import/wikipedia1000")));
//        	br = new BufferedReader(new FileReader(new File("import/wikipedia10000")));
//        	br = new BufferedReader(new FileReader(new File("import/enwiki-20120502-all-titles-in-ns0")));
            String line = null;
            while((line = br.readLine()) != null) {            
                String[] parts = line.split("_");
                for (int i = 0; i < parts.length; i++) {
                	if (!parts[i].startsWith("-")) {
                		myDC.insert(parts[i], line);
//	                	wiki += parts[i];
//	                	wiki += '\n';
                	}
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    	
        return wiki;
	}
	
	
	@Override
	public void cliEnterLoop() {
		readWikipedia();
	}

	
	@Override
	public void cliLeaveLoop() {
		myDC.shutdown();
	}
	
	
    @Command 
    public String wikipedia() {
    	return readWikipedia();
    }

    
    @Command 
    public String insert(String title) {         
        String[] parts = title.split("_");
        for (int i = 0; i < parts.length; i++) {
        	if (!parts[i].startsWith("-")) {
        		myDC.insert(parts[i], title);
        	}
        }
    	return title + " inserted.";
    }
    
    
    @Command 
    public String delete(String key) {
    	myDC.delete(key);
    	return key + " deleted.";
    }
    
    @Command 
    public String lookup(String key) {
    	String title = myDC.lookup(key);
        return title + " http://en.wikipedia.org/wiki/" + title;
    }

}
