package ds.data;

import java.util.Set;

public interface Node {

    String getID();

    int getNumericID();

    void insert(long key, String value);

    String lookup(long key);

    void delete(long key);

    int size();

    Set<Long> getKeys();

    Set<Node> getLocalNodes();

    Set<ReplicatedNode> getReplicationNodes();
}
