package ds.data;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.rmi.RemoteObject;
import ds.dc.commands.DeleteDataCmd;
import ds.dc.commands.InsertDataCmd;

import java.util.*;

public class ReplicatedNode extends NodeBase {

    private Vector<Node> nodes = new Vector<Node>();

    public ReplicatedNode(final int level, final int key, Node... nodes) {
        super(level, key);

        synchronized (this) {
            for (Node node : nodes) {
                if (!(node instanceof ReplicatedNode)) {
                    this.nodes.add(node);
                }
            }
        }
    }

    public synchronized void addNode(Node node) {
        if (node instanceof RemoteObject) {
//            System.out.println("--> ADDING REMOTE NODE TO REPLICATED NODE " + this.getNumericID() + "(#" + nodes.size() + ")");
            // Make sure that the new node does not belong to an already replicated peer.
            this.removeNodeBelongingToConnection(((RemoteObject)node).getConnection());
        } else {
//            System.out.println("--> ADDING LOCAL NODE TO REPLICATED NODE " + this.getNumericID() + "(#" + nodes.size() + ")");
        }

        nodes.add(node);
    }

    public synchronized void removeNodeBelongingToConnection(Connection connection) {
        Iterator<Node> nodeIter = nodes.iterator();
        while(nodeIter.hasNext()) {
            Node node = nodeIter.next();
            if (node instanceof RemoteObject) {
                if (((RemoteObject) node).getConnection().equals(connection)) {
//                    System.out.println("REMOVING NODE " + getNumericID() + " BELONGING TO " + connection);
                    nodeIter.remove();
                }
            }
        }
    }

    public synchronized int getReplicationCount() {
        return nodes.size();
    }

    public synchronized boolean hasLocalNode() {
        for (Node node : nodes) {
            if (!(node instanceof RemoteObject)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public synchronized void insert(long key, String value) {
        Node randomNode = selectNode();
        if (randomNode instanceof RemoteObject || randomNode instanceof InnerNode) {
//            System.out.println("--> NODE " + this.getNumericID() + " FORWARDING INSERT");
            randomNode.insert(key, value);
        } else {
            // if replicated node belongs to a leaf node than the operation must be replicated to all other peers replicating this one
            for (Node node : nodes) {
                if (node instanceof RemoteObject) {
                    RemoteObject remoteNode = (RemoteObject)node;
//                    System.out.println("--> INSERTING INTO REMOTE NODE " + remoteNode.getConnection().getRemoteAddressTCP());
                    remoteNode.getConnection().sendTCP(new InsertDataCmd(key, value));
                } else {
//                    System.out.println("--> INSERTING INTO LOCAL NODE " + this.getNumericID());
                    node.insert(key, value);
                }
            }
        }
    }

    @Override
    public synchronized String lookup(long key) {
//        System.out.println("--> LOOKING UP " + key);

        String value = selectNode().lookup(key);

//        System.out.println("--> LOOKING UP " + key + " DONE");
        return value;
    }

    @Override
    public synchronized void delete(long key) {
        Node randomNode = selectNode();
        if (randomNode instanceof RemoteObject || randomNode instanceof InnerNode) {
//            System.out.println("--> NODE " + this.getNumericID() + " INSERTING INTO REMOTE NODE");
            randomNode.delete(key);
        } else {
            // if replicated node belongs to a leaf node than the operation must be replicated to all other nodes
            assert(randomNode instanceof LeafNode);

//           System.out.println("--> REPLICATING");
           for (Node node : nodes) {
                if (node instanceof RemoteObject) {
                    RemoteObject remoteNode = (RemoteObject)node;
                    remoteNode.getConnection().sendTCP(new DeleteDataCmd(key));
                } else {
                    node.delete(key);
                }
            }
        }
    }

    @Override
    public synchronized int size() {
        return selectNode().size();
    }

    @Override
    public synchronized Set<Long> getKeys() {
        return selectNode().getKeys();
    }

    @Override
    public synchronized Set<Node> getLocalNodes() {
        for (Node node : nodes) {
            if (node instanceof InnerNode || node instanceof LeafNode) {
                return node.getLocalNodes();
            }
        }

        // If no local nodes exist for this node return an empty set
        return new HashSet<Node>();
    }

    @Override
    public synchronized Set<ReplicatedNode> getReplicationNodes() {
        Set<ReplicatedNode> replicationNodes = new HashSet<ReplicatedNode>();
        replicationNodes.add(this);
        for (Node node : nodes) {
            if (!(node instanceof RemoteObject)) {
                replicationNodes.addAll(node.getReplicationNodes());
            }
        }
        return replicationNodes;
    }

    private Random random = new Random();

    private Node selectNode() {
        for (Node node : nodes) {
            if (!(node instanceof RemoteObject)) {
                return node;
            }
        }

        return nodes.get(random.nextInt(nodes.size() - 1));
    }
}
