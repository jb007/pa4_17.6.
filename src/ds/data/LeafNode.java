package ds.data;

import java.util.*;

public class LeafNode extends NodeBase {

    private Map<Long, String> data = new HashMap<Long, String>();

    public LeafNode(final int level, final int key) {
        super(level, key);
    }

    @Override
    public void insert(long key, String value) {
//        System.out.println("--> INSERTING " + key);

        assertValidKeyInterval(key);

        data.put(key, value);
    }

    @Override
    public String lookup(long key) {
        assertValidKeyInterval(key);

        return data.get(key);
    }

    @Override
    public void delete(long key) {
//        System.out.println("--> DELETING " + key);

        assertValidKeyInterval(key);

        data.remove(key);
    }

    @Override
    public int size() {
        return data.size();
    }

    @Override
    public Set<Long> getKeys() {
        return new HashSet<Long>(data.keySet());
    }

    @Override
    public Set<Node> getLocalNodes() {
        HashSet<Node> nodes = new HashSet<Node>();
        nodes.add(this);
        return nodes;
    }

    @Override
    public Set<ReplicatedNode> getReplicationNodes() {
        return new HashSet<ReplicatedNode>();
    }
}
