package ds.data;

import java.util.HashSet;
import java.util.Set;

public class InnerNode extends NodeBase {

    Node left;

    Node right;

    public InnerNode(final int level, final int key, Node left, Node right) {
        super(level, key);

        this.left = left;
        this.right = right;
    }

    @Override
    public void insert(long key, String value) {
        assertValidKeyInterval(key);

        getChildForKey(key).insert(key, value);
    }

    @Override
    public String lookup(long key) {
        assertValidKeyInterval(key);

        return getChildForKey(key).lookup(key);
    }

    @Override
    public void delete(long key) {
        assertValidKeyInterval(key);

        getChildForKey(key).delete(key);
    }

    private Node getChildForKey(long key) {
        if ((key >>> level & 0x1) == 0x0) {
            return left;
        } else {
            return right;
        }
    }

    @Override
    public int size() {
        return left.size() + right.size();
    }

    public Set<Long> getKeys() {
        Set<Long> keys = new HashSet<Long>();
        keys.addAll(left.getKeys());
        keys.addAll(right.getKeys());
        return keys;
    }

    @Override
    public Set<Node> getLocalNodes() {
        HashSet<Node> nodes = new HashSet<Node>();
        nodes.add(this);
        nodes.addAll(left.getLocalNodes());
        nodes.addAll(right.getLocalNodes());
        return nodes;
    }

    @Override
    public Set<ReplicatedNode> getReplicationNodes() {
        HashSet<ReplicatedNode> nodes = new HashSet<ReplicatedNode>();
        nodes.addAll(left.getReplicationNodes());
        nodes.addAll(right.getReplicationNodes());
        return nodes;

    }

    public boolean hasBothLocalChildren() {
        if (left instanceof ReplicatedNode && !((ReplicatedNode) left).hasLocalNode()) return false;
        if (right instanceof ReplicatedNode && !((ReplicatedNode) right).hasLocalNode()) return false;
        return true;
    }
}
