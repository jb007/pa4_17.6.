package ds.data;

public abstract class NodeBase implements Node {

    protected final int level;

    protected final int key;

    public NodeBase(final int level, final int key) {
        this.level = level;
        this.key = key;
    }

    public String getID() {
        if (level == 0) {
            return "";
        } else {
            return String.format("%" + level + "s", Integer.toBinaryString(key)).replace(' ', '0');
        }
    }

    public int getNumericID() {
        return level << 8 | key;
    }

    public static int keyFromNumericID(int ID) {
        return ID & 0xFF;
    }

    public static int levelFromNumericID(int ID) {
        return (ID & 0xFF00) >> 8;
    }

    public static Integer getParentNumericIDForID(int ID) {
        int level = levelFromNumericID(ID);
        int key = keyFromNumericID(ID);

        if (level == 0) {
            return null;
        } else {
            return (level - 1) << 8 | (key & (0xFF >> (8 - (level - 1))));
        }
    }

    public Integer getParentNumericID() {
        if (level == 0) {
            return null;
        } else {
            return NodeBase.getParentNumericIDForID(this.getNumericID());
        }
    }

    private static final long MIN_KEY = 0;

    private static final long MAX_KEY = Math.round(Math.pow(2, 32));

    public void assertValidKeyInterval(long key) {
        if (level == 0) {
            if (key < MIN_KEY || key > NodeBase.MAX_KEY) {
                throw new RuntimeException("key (" + key + ") is not within range [0, 2^32]");
            }
        } else {
            long bitmask = 0;
            for (int i = 0; i < level; i++) {
                bitmask += Math.pow(2, i);
            }
            if ((key & bitmask) != this.key) {
                System.out.println(Long.toBinaryString(key & bitmask));
                throw new RuntimeException("invalid key (" + Long.toBinaryString(key) + ") for node " + this.getID());
            }
        }
    }
}
